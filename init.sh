#!/bin/bash


# load aliases

ALIASES_FILE=~/.aliases
if [[ -f "$ALIASES_FILE" ]]; then
	source "$ALIASES_FILE"
fi

#add keyboard layout
setxkbmap -model pc104 -layout ir,us -variant -option grp:alt_shift_toggle

export TERM=xterm-256color
export PATH="$HOME/.cargo/bin:$PATH"

