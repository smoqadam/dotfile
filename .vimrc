set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'https://github.com/scrooloose/nerdtree'
Plugin 'https://github.com/vim-syntastic/syntastic'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
Plugin 'https://github.com/terryma/vim-multiple-cursors'
Plugin 'https://github.com/rust-lang/rust.vim'
Plugin 'https://github.com/airblade/vim-gitgutter'
Plugin 'https://github.com/tlhr/anderson.vim'
Plugin 'https://github.com/itchyny/lightline.vim'
Plugin 'https://github.com/jaredgorski/SpaceCamp'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

let g:gitgutter_async=0
let g:gitgutter_log=1
let s:grep_available=0
colorscheme spacecamp

let g:gitgutter_terminal_reports_focus=0

" lightline.vim config
set laststatus=2
if !has('gui_running')
  set t_Co=256
endif
set noshowmode

set number
syntax on
filetype plugin indent on

"autocmd vimenter * NERDTree

map <C-n> :NERDTreeToggle<CR>
set encoding=utf-8
set fileencodings=iso-2022-jp,euc-jp,sjis,utf-8
set fileformats=unix,dos,mac

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_shell = "/usr/bin/bash"
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
