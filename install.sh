#!/bin/bash

# update repository
pacman -Syu

## install xorg
sudo pacman -Syy xorg

## install softwares
pacman -S zsh vlc vim telegram-desktop sudo htop firefox code networkmanager i3-wm i3lock i3status i3blocks lightdm lightdm-gtk-greeter curl docker axel 

# install oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"


# install rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

#install docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose


# install i3blocklets
git clone https://github.com/vivien/i3blocks-contrib.git ~/.i3blocklets

# vim plugin manager
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
